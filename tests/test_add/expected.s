	.file	"test.cpp"
	.abiversion 2
	.section	".text"
	.align 2
	.p2align 4,,15
	.globl _Z10test_add_1N2sv3VecIhLm1ELm4EEES1_
	.type	_Z10test_add_1N2sv3VecIhLm1ELm4EEES1_, @function
_Z10test_add_1N2sv3VecIhLm1ELm4EEES1_:
.LFB27:
	.cfi_startproc
	li 9,4
	li 10,-1
#APP
 # 312 "include/simplev_cpp.h" 1
	# setvl 0, 9, MVL=4
	.long 1275068416 | (0 << 21) | (9 << 16) | ((4 - 1) << 9)
	# sv.add ew=8, subvl=1, m=10, 8.v, 3.v, 4.v
	.long 5186688
	add 8, 3, 4
 # 0 "" 2
#NO_APP
	mr 3,8
	blr
	.long 0
	.byte 0,9,0,0,0,0,0,0
	.cfi_endproc
.LFE27:
	.size	_Z10test_add_1N2sv3VecIhLm1ELm4EEES1_,.-_Z10test_add_1N2sv3VecIhLm1ELm4EEES1_
	.align 2
	.p2align 4,,15
	.globl _Z10test_add_2N2sv3VecItLm1ELm4EEES1_
	.type	_Z10test_add_2N2sv3VecItLm1ELm4EEES1_, @function
_Z10test_add_2N2sv3VecItLm1ELm4EEES1_:
.LFB34:
	.cfi_startproc
	li 9,4
	li 10,-1
#APP
 # 312 "include/simplev_cpp.h" 1
	# setvl 0, 9, MVL=4
	.long 1275068416 | (0 << 21) | (9 << 16) | ((4 - 1) << 9)
	# sv.add ew=16, subvl=1, m=10, 8.v, 3.v, 4.v
	.long 4859008
	add 8, 3, 4
 # 0 "" 2
#NO_APP
	mr 3,8
	blr
	.long 0
	.byte 0,9,0,0,0,0,0,0
	.cfi_endproc
.LFE34:
	.size	_Z10test_add_2N2sv3VecItLm1ELm4EEES1_,.-_Z10test_add_2N2sv3VecItLm1ELm4EEES1_
	.align 2
	.p2align 4,,15
	.globl _Z10test_add_3N2sv3VecItLm1ELm4EEES1_S1_
	.type	_Z10test_add_3N2sv3VecItLm1ELm4EEES1_S1_, @function
_Z10test_add_3N2sv3VecItLm1ELm4EEES1_S1_:
.LFB35:
	.cfi_startproc
	li 9,4
	li 10,-1
#APP
 # 312 "include/simplev_cpp.h" 1
	# setvl 0, 9, MVL=4
	.long 1275068416 | (0 << 21) | (9 << 16) | ((4 - 1) << 9)
	# sv.add ew=16, subvl=1, m=10, 8.v, 4.v, 5.v
	.long 4859008
	add 8, 4, 5
 # 0 "" 2
#NO_APP
	mr 5,8
#APP
 # 312 "include/simplev_cpp.h" 1
	# setvl 0, 9, MVL=4
	.long 1275068416 | (0 << 21) | (9 << 16) | ((4 - 1) << 9)
	# sv.add ew=16, subvl=1, m=10, 8.v, 3.v, 5.v
	.long 4859008
	add 8, 3, 5
 # 0 "" 2
#NO_APP
	mr 3,8
	blr
	.long 0
	.byte 0,9,0,0,0,0,0,0
	.cfi_endproc
.LFE35:
	.size	_Z10test_add_3N2sv3VecItLm1ELm4EEES1_S1_,.-_Z10test_add_3N2sv3VecItLm1ELm4EEES1_S1_
	.ident	"GCC"
	.section	.note.GNU-stack,"",@progbits
